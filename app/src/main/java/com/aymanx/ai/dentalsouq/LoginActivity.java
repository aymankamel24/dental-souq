package com.aymanx.ai.dentalsouq;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {


    private String specficUserName = "aymankamel24";
    private String specficPassword = "123456";

    // UI references.
    private TextView userName;
    private EditText mPassword;
    private Button mEmailSignInButton;
    private Button register;

    public CustomShare shared;


    //notification


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        register=findViewById(R.id.register);
        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

            }
        });


        userName = findViewById(R.id.username);
        mPassword = findViewById(R.id.password);

        // Set up the login form.
        mEmailSignInButton = findViewById(R.id.email_sign_in_button);

        mEmailSignInButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (userName.getText() != null && mPassword.getText() != null ) {
                    if (       userName.getText().toString().equals(specficUserName)
                            && mPassword.getText().toString().equals(specficPassword)
                                      ) {
                        startActivity(new Intent(LoginActivity.this, ShowActivity.class));
                        Toast.makeText(LoginActivity.this, "Successfully Login", Toast.LENGTH_SHORT).show();

//SharedPreferences



                    } else {
                        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(LoginActivity.this);

                        dlgAlert.setMessage("wrong UserName OR Password   ");
                        dlgAlert.setTitle("Error Message...");
                        dlgAlert.setPositiveButton("OK", null);
                        dlgAlert.setCancelable(true);
                        dlgAlert.create().show();

                        dlgAlert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                    }
                } else {
                    // The fields are not filled.
                    // Display an error message like "Please enter username/password
                    Toast.makeText(LoginActivity.this, "Please Enter Correct Data", Toast.LENGTH_SHORT).show();
                }

            }
        });




    }

}


