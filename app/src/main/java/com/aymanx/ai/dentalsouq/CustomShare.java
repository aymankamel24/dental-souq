package com.aymanx.ai.dentalsouq;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class CustomShare {
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    public CustomShare(Context context) {

        sharedPreferences = context.getSharedPreferences("Ayman",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    public void loginStatus(boolean islogin){

        editor.putBoolean("login",islogin);
    }


    public boolean isLogin(){
        return sharedPreferences.getBoolean("login",false);

    }

    public void stData(String FirstName,String LastName,String uUserNameD,String Email,String Phone,String Password){

        editor.putString("FirstName",FirstName);
        editor.putString("LastName",LastName);
        editor.putString("userNameD",uUserNameD);
        editor.putString("Email",Email);
        editor.putString("Phone",Phone);
        editor.putString("Password",Password);
        editor.commit();
    }

    public HashMap<String,Object> gtData(){
        HashMap<String,Object> map = new HashMap<>();
            map.put("FirstName",sharedPreferences.getString("FirstName",""));
            map.put("LastName",sharedPreferences.getString("LastName",""));
            map.put("userNameD",sharedPreferences.getString("userNameD",""));
            map.put("Email",sharedPreferences.getString("Email",""));
            map.put("Phone",sharedPreferences.getString("Phone",""));
            map.put("Password",sharedPreferences.getString("Password",""));

        return map;
    }

}
