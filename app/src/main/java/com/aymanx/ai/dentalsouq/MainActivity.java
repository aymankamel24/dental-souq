package com.aymanx.ai.dentalsouq;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button sent;
    EditText mail;
    ImageButton call;
    TextView number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mail = findViewById(R.id.ed_mail);
        final String mails = mail.getText().toString();
        sent = findViewById(R.id.bt_sentmail);

        sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
               // i.setType("text/html");

                i.setAction(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { "islam.mahmoud116@gmail.com" });
                i.putExtra(android.content.Intent.EXTRA_SUBJECT," Test Intent");
                i.putExtra(Intent.EXTRA_TEXT, "Test form my pc");
                i.setType("text/html");
                startActivity(i);
            }
        });

        number = findViewById(R.id.number);
        final String numbers = number.getText().toString();

        call = findViewById(R.id.ib_call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent z = new Intent(Intent.ACTION_DIAL);
                    z.setData(Uri.parse("tel:"+numbers));
                    startActivity(z);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });





    }

    private boolean isTelephonyEnabled(){
        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        return tm != null && tm.getSimState()==TelephonyManager.SIM_STATE_READY;}


}
