package com.aymanx.ai.dentalsouq;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class ShowActivity extends AppCompatActivity {

    public TextView show;
    public CustomShare shared;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        shared = new CustomShare(this);


          show = findViewById(R.id.show);
        show.setText("your Saved Data"+shared.gtData());




    }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {

            MenuInflater inflater = getMenuInflater();


            inflater.inflate(R.menu.menu_of_show, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {

            switch (item.getItemId()) {

                case R.id.contactus:
                    Intent age = new Intent(ShowActivity.this, MainActivity.class);
                    startActivity(age);
                    break;



                case R.id.back_to_login:
                    Intent i = new Intent(ShowActivity.this, LoginActivity.class);
                    startActivity(i);
                    break;

                case R.id.action_finish:
                    this.finish();
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                    break;


            }

            return true;
        }

}
