package com.aymanx.ai.dentalsouq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    private EditText firstname;
    private EditText lastname;
    private EditText userNameD;
    private EditText email;
    private EditText phone;
    private EditText password;
    private EditText confirmpassword;
    private Button registered;

    public CustomShare shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        firstname=findViewById(R.id.firstname);
        lastname=findViewById(R.id.lastname);
        userNameD=findViewById(R.id.usernameD);
        email=findViewById(R.id.email);
        phone=findViewById(R.id.phone);
        password=findViewById(R.id.firstname);
        confirmpassword=findViewById(R.id.confirmpassword);



        shared = new CustomShare(this);

        shared.isLogin();
        shared.loginStatus(true);



        registered=findViewById(R.id.saveregister);
        registered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userFirstName = firstname.getText().toString();
                String userLastName = lastname.getText().toString();
                String uUserNameD = userNameD.getText().toString();
                String userEmail = email.getText().toString();
                String userPhone = phone.getText().toString();
                String userPassword = password.getText().toString();

                shared.stData( userFirstName, userLastName,uUserNameD,userEmail,userPhone,userPassword);


                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));

                 Toast.makeText(RegisterActivity.this, "Account Created Sucessfuly ", Toast.LENGTH_SHORT).show();

            }
        });





    }
}
